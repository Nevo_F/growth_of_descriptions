import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import string

PATH_TO_DF = os.path.abspath('descriptions_growth_90d.csv')

df = pd.read_csv(PATH_TO_DF)

df['series_description'] = [str(s).translate(str.maketrans('', '', string.punctuation + ' ')).upper()
                            for s in df['series_description']]
df['study_description'] = [str(s).translate(str.maketrans('', '', string.punctuation + ' ')).upper()
                           for s in df['study_description']]

df['tuples'] = list(zip(df.study_description, df.series_description))


dates = df['date'].unique()
dates = sorted(dates)
redundant_dates = [dates[i] for i in np.arange(0, 90, 3)]

nunique_series_description_upto_given_date = \
    np.array([df[df['date'] <= redundant_dates[i]].nunique().loc['series_description']
              for i in range(len(redundant_dates))])

nunique_studies_description_upto_given_date = \
    np.array([df[df['date'] <= redundant_dates[i]].nunique().loc['study_description']
              for i in range(len(redundant_dates))])

nunique_tuples_upto_given_date = \
    np.array([df[df['date'] <= redundant_dates[i]].nunique().loc['tuples'] for i in range(len(redundant_dates))])


diff_series = np.insert(np.diff(nunique_series_description_upto_given_date), 0,
                        nunique_series_description_upto_given_date[0])
diff_studies = np.insert(np.diff(nunique_studies_description_upto_given_date), 0,
                        nunique_studies_description_upto_given_date[0])
diff_tuples = np.insert(np.diff(nunique_tuples_upto_given_date), 0,
                        nunique_tuples_upto_given_date[0])


fig1, ax1 = plt.subplots()


labels = redundant_dates
width = 0.35


ax1.bar(labels, nunique_series_description_upto_given_date - diff_series, width,
        label='Aggregated unique series descriptions')
ax1.bar(labels, diff_series, width, bottom=nunique_series_description_upto_given_date - diff_series,
        label='Daily added unique series descriptions')

ax1.set_ylabel('#')
ax1.set_title('A count of unique series descriptions aggregated along the past 90d')
ax1.legend()
plt.xticks(rotation=90)
plt.show()

fig2, ax2 = plt.subplots()


ax2.bar(labels, nunique_studies_description_upto_given_date - diff_studies, width,
        label='Aggregated unique study descriptions')
ax2.bar(labels, diff_studies, width, bottom=nunique_studies_description_upto_given_date - diff_studies,
        label='Daily added unique study descriptions')

ax2.set_ylabel('#')
ax2.set_title('A count of unique study descriptions aggregated along the past 90d')
ax2.legend()
plt.xticks(rotation=90)
plt.show()

fig3, ax3 = plt.subplots()


ax3.bar(labels, nunique_tuples_upto_given_date - diff_tuples, width,
        label='Aggregated unique (study, series) descriptions')
ax3.bar(labels, diff_series, width, bottom=nunique_tuples_upto_given_date - diff_tuples,
        label='Daily added unique (study, series) descriptions')

ax3.set_ylabel('#')
ax3.set_title('A count of unique (study, series) descriptions aggregated along the past 90d')
ax3.legend()
plt.xticks(rotation=90)
plt.show()
