SELECT trunc(_timestamp) as date, aidoc_series_uid, study_description, series_description
FROM production_metrics.series_metadata
WHERE aidoc_site = 'yale' and date >= dateadd(day, -90, getdate());
-- ORDER BY date DESC;